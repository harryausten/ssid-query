use std::{
    ffi::CStr,
    os::{fd::AsRawFd, raw::c_char},
};

use anyhow::bail;
use clap::Parser;
use futures::TryStreamExt;
use nix::sys::socket::{self, AddressFamily, SockFlag, SockType};
use wl_nl80211::Nl80211Attr;

#[repr(C)]
pub struct Essid {
    pointer: *const c_char,
    length: u16,
    flags: u16,
}

impl Essid {
    const fn new(pointer: *const c_char) -> Self {
        Self {
            pointer,
            length: 33,
            flags: 0,
        }
    }
}

#[repr(C)]
pub struct WreqData {
    essid: Essid,
}

#[repr(C)]
pub struct Iwreq {
    frn_name: [u8; 16],
    u: WreqData,
}

impl Iwreq {
    fn new(name: &str, buf: &[c_char; 33]) -> Self {
        let mut frn_name = [0; 16];
        frn_name[..name.len()].copy_from_slice(name.as_bytes());

        Self {
            frn_name,
            u: WreqData {
                essid: Essid::new(buf.as_ptr()),
            },
        }
    }
}

nix::ioctl_read_bad!(read_essid, 0x8B1B, Iwreq);

fn ioctl_method(name: &str) -> anyhow::Result<String> {
    let sock = socket::socket(
        AddressFamily::Inet,
        SockType::Datagram,
        SockFlag::empty(),
        None,
    )?;
    let buf = [0; 33];
    let mut data = Iwreq::new(name, &buf);
    unsafe { read_essid(sock.as_raw_fd(), &mut data) }?;
    let ptr = buf.as_ptr();
    Ok(unsafe { CStr::from_ptr(ptr) }.to_str()?.to_owned())
}

async fn netlink_method(name: &str) -> anyhow::Result<String> {
    let (connection, handle, _) = wl_nl80211::new_connection()?;
    tokio::spawn(connection);

    let mut ifaces = handle.interface().get().execute().await;
    while let Ok(Some(iface)) = ifaces.try_next().await {
        if !iface
            .payload
            .nlas
            .iter()
            .any(|nla| matches!(nla, Nl80211Attr::IfName(n) if n == name))
        {
            continue;
        }

        for nla in iface.payload.nlas {
            if let Nl80211Attr::Ssid(ssid) = nla {
                return Ok(ssid);
            }
        }
    }

    bail!("SSID not found")
}

#[derive(Parser)]
struct Args {
    name: String,
    #[arg(short, long)]
    ioctl: bool,
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let args = Args::parse();
    let name = &args.name;

    let ssid = if args.ioctl {
        ioctl_method(name)?
    } else {
        netlink_method(name).await?
    };

    println!(" {ssid}");
    Ok(())
}
